#!/bin/bash

PLAYDIR=$1
INVENTORY=$2
GROUP=$3
ETCOPT=${@:4}

cd ${PLAYDIR}
ansible-playbook ${ETCOPT} -i ${INVENTORY}.yml ${GROUP}.yml


exit 0
