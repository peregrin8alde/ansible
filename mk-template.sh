#!/bin/sh

SCRIPT_DIR=$(cd $(dirname $0) ; pwd)

PLAYDIR=$1

mkdir -p ${PLAYDIR}
cd ${PLAYDIR}

cp -rpf ${SCRIPT_DIR}/sample/* ./

# inventory file for production servers
cat <<'EOF' | tee production.yml > /dev/null
all:
  hosts:
    localhost:
  children:
    grp01:
      hosts:
        localhost:
EOF

# inventory file for staging environment
cp -p production.yml staging.yml

mkdir group_vars
touch group_vars/grp01.yml

mkdir host_vars

# master playbook
cat <<'EOF' | tee site.yml > /dev/null
---
- import_playbook: grp01.yml
EOF

# playbook for webserver tier
cat <<'EOF' | tee grp01.yml > /dev/null
---
- hosts: grp01
  roles:
    - common
EOF

mkdir roles
cd roles

ansible-galaxy init --offline common

exit 0