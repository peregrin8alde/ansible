# 動作確認用sshコンテナ

参考：https://hub.docker.com/_/centos

```
docker build --rm -t local/c7-systemd base
docker build --rm -t local/c7-systemd-sshd sshd

docker run -d \
  --name ssh1 \
  --hostname ssh1 \
  -p 10022:22 \
  -v /sys/fs/cgroup:/sys/fs/cgroup:ro \
  local/c7-systemd-sshd

# ubuntuの場合
docker run -d \
  --name ssh1 \
  --hostname ssh1 \
  -p 10022:22 \
  -v /sys/fs/cgroup:/sys/fs/cgroup:ro \
  -v $(mktemp -d):/run \
  local/c7-systemd-sshd
# nologinが存在するとroot以外でログインできなくなる
docker exec ssh1 rm -rf /run/nologin

docker exec -it ssh1 \
  sudo -u docker sh -c "echo $(cat ~/.ssh/id_rsa.pub) >> /home/docker/.ssh/authorized_keys"

ssh -p 10022 -i ~/.ssh/id_rsa -o StrictHostKeyChecking=no docker@localhost
```
